import operator
from functools import reduce
from itertools import groupby

import requests
from flask import Flask

app = Flask(__name__)

istanbul_bounds = '40.5660,27.7322,41.8061,30.3635'
activity_type = 'riding'
access_token = '1229559019f835ce93529115189dc3cc13b0f892'
leaderboard_per_page = 50

strava_url_prefix = 'https://www.strava.com/api/v3/segments'


def get_top_segments():
    payload = {'bounds': istanbul_bounds, 'activity_type': activity_type, 'access_token': access_token}
    try:
        r = requests.get(strava_url_prefix + '/explore', params=payload)
        segments_dict = r.json()
        return segments_dict['segments']
    except Exception as e:
        print('Exception when calling top 10 segments: %s\n' % e)
        raise e


def get_segment_ids():
    return map(lambda segment: segment['id'], get_top_segments())


def get_leaderboard_by_segment_id(segment_id):
    payload = {'per_page': leaderboard_per_page, 'access_token': access_token}
    try:
        r = requests.get(strava_url_prefix + '/{}/leaderboard'.format(segment_id), params=payload)
        leaderboard_dict = r.json()
        return leaderboard_dict['entries']
    except Exception as e:
        print('Exception when calling leader board: {}\nSegment Id: {}'.format(e, segment_id))
        raise e


def get_rider_name_and_appeared_count(rider_name, rider_entries):
    return rider_name + ': ' + str(sum(1 for _ in rider_entries))


def get_result_string(result_str, rider_dict):
    return result_str + reduce(get_rider_name_and_appeared_count, rider_dict) + '<br/>'


def get_high_score_result_string(result_str, rider_dict):
    return result_str + rider_dict['athlete_name'] + ': ' + str(rider_dict['high_score']) + '<br/>'


def has_multiple_leaderboard_result(rider_entry_tuple):
    return sum(1 for _ in rider_entry_tuple[1]) > 1


def get_athlete_name(dict):
    return dict['athlete_name']


def concat_items(list_of_items):
    return reduce(operator.add, list_of_items)


def group_by_sorted_athlete_name(dicts):
    return groupby(sorted(dicts, key=get_athlete_name), get_athlete_name)


def iter_dict_to_dict(dict):
    return [(key, list(group)) for key, group in dict]


def calculate_score(distance, elev_difference, athlete_entry):
    return round((elev_difference + 1) *
                 (distance / (athlete_entry['elapsed_time'] - athlete_entry['moving_time'] + 1)) *
                 (100 / athlete_entry['elapsed_time']), 2)


def calculate_high_score_by_segment(segment):
    segment_id = segment['id']
    distance = segment['distance']
    elev_difference = segment['elev_difference']
    leaderboard_riders = get_leaderboard_by_segment_id(segment_id)
    return list(map(lambda e: {'high_score': calculate_score(distance, elev_difference, e),
                               'athlete_name': e['athlete_name']}, leaderboard_riders))


def sum_high_score_segments_of_rider(rider_segments):
    return reduce(lambda total, e: total + e['high_score'], rider_segments, 0)


def get_multiple_appeared_riders(leaderboard_riders):
    flatten_data_riders = reduce(operator.add, leaderboard_riders)
    group_by_riders = groupby(sorted(flatten_data_riders, key=get_athlete_name), get_athlete_name)
    rider_entries_dict = iter_dict_to_dict(group_by_riders)  # be able to iterate more than once
    return filter(has_multiple_leaderboard_result, rider_entries_dict)


def riders_in_multiple_leaderboards():
    leaderboard_riders = map(get_leaderboard_by_segment_id, get_segment_ids())
    multiple_appeared_riders = get_multiple_appeared_riders(leaderboard_riders)

    return reduce(get_result_string, multiple_appeared_riders, '')


def high_scores():
    segments = get_top_segments()
    athletes_with_high_scores = map(calculate_high_score_by_segment, segments)
    concatenated_items = concat_items(athletes_with_high_scores)
    grouped_items_by_name = group_by_sorted_athlete_name(concatenated_items)
    rider_high_score_dict = iter_dict_to_dict(grouped_items_by_name)  # be able to iterate more than once
    riders_with_high_scores = map(lambda rider: {'athlete_name': rider[0],
                                                 'high_score': sum_high_score_segments_of_rider(rider[1])},
                                  rider_high_score_dict)

    riders_sorted_by_high_score = sorted(riders_with_high_scores, key=lambda x: x['high_score'], reverse=True)

    return reduce(get_high_score_result_string, riders_sorted_by_high_score, '')
