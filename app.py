import rider

from flask import Flask

app = Flask(__name__)


@app.route('/riders_in_multiple_leaderboards')
def riders_in_multiple_leaderboards():
    return rider.riders_in_multiple_leaderboards()


@app.route('/high_scores')
def high_scores():
    return rider.high_scores()


if __name__ == '__main__':
    app.run()
