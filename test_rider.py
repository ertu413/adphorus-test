import unittest, rider

segments = [{'id': 100,
             'distance': 225.2,
             'name': 'Kartepe Climb',
             'elev_difference': 1228},
            {'id': 200,
             'distance': 345.62,
             'name': 'Ortakoy - Bebek',
             'elev_difference': 37.5},
            {'id': 300,
             'distance': 445.11,
             'name': 'Beykoz Sahil',
             'elev_difference': 77.1}]

ahmet_segment_id_100_entry = {'segment_id': 100,
                              'athlete_name': 'Ahmet E.',
                              'elapsed_time': 485,
                              'moving_time': 485}
can_segment_id_100_entry = {'segment_id': 100,
                            'athlete_name': 'Can K.',
                            'elapsed_time': 555,
                            'moving_time': 555}
ahmet_segment_id_200_entry = {'segment_id': 200,
                              'athlete_name': 'Ahmet E.',
                              'elapsed_time': 689,
                              'moving_time': 689}
can_segment_id_200_entry = {'segment_id': 200,
                            'athlete_name': 'Can K.',
                            'elapsed_time': 685,
                            'moving_time': 685}
can_segment_id_300_entry = {'segment_id': 300,
                            'athlete_name': 'Can K.',
                            'elapsed_time': 1288,
                            'moving_time': 1102}
selin_segment_id_300_entry = {'segment_id': 300,
                              'athlete_name': 'Selin L.',
                              'elapsed_time': 1892,
                              'moving_time': 1789}

leaderboard_entries = [[ahmet_segment_id_100_entry, can_segment_id_100_entry],
                       [ahmet_segment_id_200_entry, can_segment_id_200_entry],
                       [can_segment_id_300_entry, selin_segment_id_300_entry]]


class TestRiderMethods(unittest.TestCase):

    def test_leaderboard(self):
        self.assertEqual(list(rider.get_multiple_appeared_riders(leaderboard_entries)),
                         [('Ahmet E.',
                           [{'athlete_name': 'Ahmet E.',
                             'elapsed_time': 485,
                             'moving_time': 485,
                             'segment_id': 100},
                            {'athlete_name': 'Ahmet E.',
                             'elapsed_time': 689,
                             'moving_time': 689,
                             'segment_id': 200}]),
                          ('Can K.',
                           [{'athlete_name': 'Can K.',
                             'elapsed_time': 555,
                             'moving_time': 555,
                             'segment_id': 100},
                            {'athlete_name': 'Can K.',
                             'elapsed_time': 685,
                             'moving_time': 685,
                             'segment_id': 200},
                            {'athlete_name': 'Can K.',
                             'elapsed_time': 1288,
                             'moving_time': 1102,
                             'segment_id': 300}])])

    def test_calculate_ahmet_high_scores(self):
        self.assertEqual(
            rider.calculate_score(segments[0]['distance'], segments[0]['elev_difference'], ahmet_segment_id_100_entry),
            57066.14)
        self.assertEqual(
            rider.calculate_score(segments[1]['distance'], segments[1]['elev_difference'], ahmet_segment_id_200_entry),
            1931.26)

    def test_calculate_can_high_scores(self):
        self.assertEqual(
            rider.calculate_score(segments[0]['distance'], segments[0]['elev_difference'], can_segment_id_100_entry),
            49868.61)
        self.assertEqual(
            rider.calculate_score(segments[1]['distance'], segments[1]['elev_difference'], can_segment_id_200_entry),
            1942.54)
        self.assertEqual(
            rider.calculate_score(segments[2]['distance'], segments[2]['elev_difference'], can_segment_id_300_entry),
            14.43)

    def test_calculate_selin_high_scores(self):
        self.assertEqual(
            rider.calculate_score(segments[2]['distance'], segments[2]['elev_difference'], selin_segment_id_300_entry),
            17.67)


if __name__ == '__main__':
    unittest.main()
