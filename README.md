#Installation

We are using Python 3.

`pip install Flask`

`export FLASK_APP=app.py`

`flask run`

* Running on http://127.0.0.1:5000/

* Endpoints: `http://127.0.0.1:5000/riders_in_multiple_leaderboards` 
             `http://127.0.0.1:5000/high_scores`